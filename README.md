# `my_println` as a proc macro (using `proc-macro-hack`)

This is a Cargo [Workspace][workspaces] that contains:
 * `my_println_impl` proc-macro definition
 * `my_println` proc-macro-hack *exporter* crate
 * `sample` binary crate

This is part of my exploration into Rust Macros. The full slide deck can be
found [here][hello-rust-macros].

Note: unfortunately by default Rust does not allow procedural macros to expand
to statements. There is an unstable feature flag `proc_macro_hygiene` that
enables that capability. See [my-println-proc-macro][println-proc-macro], which
requires that feature in order to be used. This macro instead uses
[proc-macro-hack][hack], created by the brilliant David Tolnay.

see: sample/src/main.rs

```rs
use my_println::my_println;

fn main() {
    my_println!();
    my_println!("Hello, world!");
    my_println!("Hello, {}!", "PDX Rust");
}
```

[workspaces]: https://doc.rust-lang.org/book/ch14-03-cargo-workspaces.html
[hello-rust-macros]: https://gitlab.com/rcousineau/hello-rust-macros
[println-proc-macro]: https://gitlab.com/rcousineau/my-println-proc-macro
[hack]: https://github.com/dtolnay/proc-macro-hack
