extern crate proc_macro;

use proc_macro::TokenStream;
use proc_macro_hack::proc_macro_hack;
use quote::quote;
use syn::{
    parse::{Parse, ParseStream},
    parse_macro_input,
    punctuated::Punctuated,
    Expr, Result as ParseResult, Token,
};

struct MyPrintlnArgs {
    args: Punctuated<Expr, Token![,]>,
}

impl Parse for MyPrintlnArgs {
    fn parse(input: ParseStream) -> ParseResult<Self> {
        let args = Punctuated::parse_terminated(input)?;
        Ok(MyPrintlnArgs { args })
    }
}

#[proc_macro_hack]
pub fn my_println(input: TokenStream) -> TokenStream {
    let parsed = parse_macro_input!(input as MyPrintlnArgs);
    let is_empty = parsed.args.is_empty();
    let args = parsed.args.into_iter();

    let simple_print = quote! {
        fn simple_print(text: &[u8]) {
            use std::io::*;
            std::io::stdout().write(text).expect("failed to write to stdout");
        }
    };

    let result = if is_empty {
        quote! {
            {
                #simple_print
                simple_print(b"<blank line>\n");
            }
        }
    } else {
        quote! {
            {
                #simple_print
                simple_print(format!(#(#args),*).as_bytes());
                simple_print(b"\n");
            }
        }
    };

    TokenStream::from(result)
}
