use my_println::my_println;

fn main() {
    my_println!();
    my_println!("Hello, world!");
    my_println!("Hello, {}!", "PDX Rust");
}
